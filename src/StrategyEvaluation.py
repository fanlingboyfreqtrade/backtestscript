import os
import json
import statistics
import argparse


def main_work_flow(backtest_result_filepath, output_path, output_filename):
    # get performance for all strategies
    output_evaluation_data_to_json(get_all_strategy_performance(backtest_result_filepath), output_path, output_filename)


def get_all_strategy_performance(backtest_result_filepath):
    all_performance_dict = {}
    # loop folder, get performance for each strategy
    # list all file end with .json but .meta.json, also without .last_result.json
    backtest_result_filename_list = [filename for filename in os.listdir(backtest_result_filepath)
                                     if filename.endswith('.json')
                                     and not filename.endswith('.meta.json')
                                     and not filename.endswith('.last_result.json')]
    for filename in backtest_result_filename_list:
        with open(backtest_result_filepath + "/" + filename) as file:
            data = json.load(file)
            for strategy in data["strategy"]:
                all_performance_dict[strategy] = get_performance(data['strategy'][strategy])
    return get_mean_sorted_performance(all_performance_dict)


def get_performance(strategy_result):
    strategy_performance = {}
    strategy_performance["profit_stats"] = get_profit_stats(strategy_result["trades"])
    strategy_performance["is_valid"] = strategy_performance["profit_stats"]['stdev'] != -1
    return strategy_performance


def get_profit_stats(trades):
    stats = {'mean': -1, 'median': -1, 'stdev': -1, 'total_count': -1}
    profit_ratio = []
    for trade in trades:
        profit_ratio.append(trade["profit_ratio"])

    stats['total_count'] = len(profit_ratio)
    if len(profit_ratio) > 2:
        stats['mean'] = statistics.mean(profit_ratio)
        stats['median'] = statistics.median(profit_ratio)
        stats['stdev'] = statistics.stdev(profit_ratio)
    return stats


def get_mean_sorted_performance(all_performance):
    sorted_all_performance_key = sorted(all_performance, key=lambda i: all_performance[i]['profit_stats']['mean'],
                                        reverse=True)
    sorted_dict = {i: all_performance[i] for i in sorted_all_performance_key}
    return sorted_dict


def output_evaluation_data_to_json(evaluation_data, output_path, output_filename):
    full_outfile_path = output_path + "/" + output_filename
    os.makedirs(os.path.dirname(full_outfile_path), exist_ok=True)
    with open(full_outfile_path, "w") as outfile:
        json.dump(evaluation_data, outfile)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--path', required=True, help='Path to the result file')
    parser.add_argument('-o', '--output-path', required=False, help='Path for outputting json file')
    parser.add_argument('-f', '--output-filename', required=True, help='filename for the json file')
    args = parser.parse_args()
    return args


def main():
    try:
        args = parse_args()
        main_work_flow(args.path, args.output_path, args.output_filename)
    except Exception as ex:
        print(ex)


if __name__ == '__main__':
    main()
